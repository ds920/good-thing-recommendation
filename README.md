### 

webdav://共享文件夹/D盘办公/本人/博客/好物推荐.docx

好物推荐
本文网址链接:
https://docs.qq.com/doc/DSUNubG5UbFVJcGpy
https://blog.51cto.com/ds920/4980893
https://www.cnblogs.com/delphixx/p/15859189.html
https://gitee.com/ds920/good-thing-recommendation/blob/master/README.md

人的生命只有一次，人一生的时间是有限的。
在每个人的一生中都有自己喜欢的一些美好的事物和朋友。
我现在要向大家推荐我发现的我比较喜欢的一些物品（硬件和软件）。
此文中推荐的物品只表示我比较喜欢，并不代表各位读者也喜欢，
也许有更好的选择，也许这些物品并不适合各位，是否购买自己决定。
我建议各位读者购买价格比较高的，销量比较大的，口碑比较好的产品。

一、生产生活娱乐硬件推荐
1、产品名称：Garmin佳明Fenix6xpro太阳能运动手表
参考价格：大约人民币8000元左右
推荐理由：稳定、长续航、坚固耐用寿命长
功能作用：记录和分析运动和睡眠数据、计时、时间提醒、音乐、支付
![输入图片说明](01-Garmin%E4%BD%B3%E6%98%8EFenix6xpro%E5%A4%AA%E9%98%B3%E8%83%BD%E8%BF%90%E5%8A%A8%E6%89%8B%E8%A1%A8.png)

2、产品名称：小米12手机12GB+256GB
参考价格：大约人民币4500元左右
推荐理由：高速、稳定、大容量、易用
功能作用：移动通讯联系、移动办公、娱乐
![输入图片说明](02-%E5%B0%8F%E7%B1%B312-12GB+256GB.png)

3、产品名称：向日葵控控A2
参考价格：大约人民币800元左右
推荐理由：带内网穿透功能、稳定、易用、高速
功能作用：ipkvm远程控制、移动办公
![输入图片说明](03-%E5%90%91%E6%97%A5%E8%91%B5%E6%8E%A7%E6%8E%A7A2.png)

4、群晖DS920+网络存储服务器
![输入图片说明](04-ds920+.png)

5、华硕AX5400无线千兆路由器
6、联想台式电脑32GB内存256GB固态硬盘
7、华为Matebook X Pro 触屏笔记本电脑16GB+512GB
8、东风日产轩逸2022款小轿车
9、尼康D850单反相机
![输入图片说明](%E5%B0%BC%E5%BA%B7D850.png)

10、福禄克Fluke289C数字万用表
![输入图片说明](%E7%A6%8F%E7%A6%84%E5%85%8B%E4%B8%87%E7%94%A8%E8%A1%A8.png)

二、电脑应用程序软件下载中心
1、腾讯软件中心-海量软件高速下载
https://pc.qq.com/
2、ZOL下载-免费软件,绿色软件
https://xiazai.zol.com.cn/
3、软件下载行业良好口碑_中国绿色软件下载大全-太平洋软件下载中心
https://dl.pconline.com.cn/

三、电脑应用程序软件推荐
适用于微软Windows电脑操作系统的软件：
1、产品名称：Windows7Pro
参考价格：大约人民币700元左右
推荐理由：高速、稳定、易用
功能作用：管理和运行电脑硬件和软件、我的首选电脑操作系统
2、产品名称：CGI-plus
参考价格：人民币0元
推荐理由：稳定、可靠、易用
功能作用：备份与恢复电脑操作系统、应用程序软件和相关设置环境
3、产品名称：DeepFreeze Standard 冰点标准版 版本 8.63.020.5634 冰点还原精灵
参考价格：大约人民币400元左右
推荐理由：稳定、可靠、易用
功能作用：保护电脑操作系统、应用程序软件和相关设置环境
4、印象笔记
5、Everything
TotalCommander10
PrimoCache
![输入图片说明](PrimoCache.png)

腾讯文档
RaiDrive
GoodSync 10.12.0.0
![输入图片说明](GoodSync.png)

![输入图片说明](GoodSync-2.png)

Server-U
WPS Office
QQ
微信
阿里云盘
Photoshop CS6
Delphi
AutoHotkey
搜狗高速浏览器
优酷视频
爱奇艺视频
腾讯视频
酷狗音乐盒
酷我音乐盒

四、安卓手机应用程序软件推荐
适用于谷歌Android手机操作系统的软件：
ES文件浏览器
西瓜视频
手机淘宝
手机京东
手机亚马逊

五、搜索引擎
1、西瓜视频搜索引擎（可以搜索各种视频教程）
https://www.ixigua.com/?wid_try=1
2、优酷视频搜索引擎
https://www.youku.com/
3、腾讯视频搜索引擎
https://v.qq.com/
4、爱奇艺视频搜索引擎
https://www.iqiyi.com/
5、淘宝搜索引擎（可以搜索各种网购物品）
https://www.taobao.com/
6、京东搜索引擎
https://www.jd.com/
7、亚马逊搜索引擎
https://www.amazon.cn/
8、简单搜索引擎
https://www.jsousuo.com/
9、必应搜索引擎
https://cn.bing.com/?ensearch=1
10、360搜索引擎
https://www.so.com/
11、百度搜索引擎
https://www.baidu.com/
12、Yandex搜索引擎
https://yandex.com/

